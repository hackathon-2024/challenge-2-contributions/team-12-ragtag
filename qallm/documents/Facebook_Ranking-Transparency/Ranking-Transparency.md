# Resource URL: https://transparency.fb.com/features/explaining-ranking
Our approach to explaining ranking
==================================

UPDATED DECEMBER 31, 2023

Artificial intelligence (AI) systems inform the ranking of content for many experiences on Meta's products, such as viewing Facebook Feed, watching reels on Instagram or browsing Facebook Marketplace.

Developing new tools to help explain AI
---------------------------------------

As part of our ongoing commitment to transparency, we provide tools and information to help people understand Meta's technologies. AI system cards, linked below, are our solution for explaining how the AI systems in our products work. System cards provide an in-depth view into the complex world of AI systems, which are made up of many models and dynamic rules. These system cards were written in a way that can be understood by experts and non-experts alike.

How AI systems work and change over time

Each AI system has models that enable it to make multiple predictions about content that people find most relevant and valuable. These prediction models use underlying input signals to help select content that people are most likely to engage with. Input signals fall into broad categories such as the unique features of a post, how others have interacted with a post, and how people have engaged with similar content in the past. Prediction models, the predictions they make, and their input signals are dynamic. They change frequently as the system learns and improves over time and as Meta's products are modified.

What affects ranking

The ranking of posts, either higher or lower in a feed, is affected by predictions about the post, features of the content, and attributes of individuals and their interactions with Meta's products. As an example, if many people have interacted in a positive way with a post on Instagram, or with similar content, the post will appear higher in a person's feed. Alternatively, if those interactions were negative, or if a piece of content is predicted to be objectionable based on our standards, guidelines or integrity policies, the content is removed or ranked lower in the feed.

AI systems work together

Each system card describes a single AI system, and several AI systems may work together to deliver an overall product experience. In Facebook Feed, for example, one AI system ranks content from friends and Pages and groups that people are connected to, and another AI system ranks recommended content that they may be interested in from others they are not connected to. What people see in their Feed is a balanced combination of the outputs of both AI systems in addition to advertisements and additional product offerings such as groups and reels.

AI systems offer a personalized experience

People's experience with Meta's products is personalized based on their activity, because not everyone likes or dislikes the same things, and interests may change over time. Our AI systems attempt to show more of the kind of content people have shown an interest in, while creating opportunities for discovery of new things the AI system predicts they might like.

People can influence their experience

System cards describe some of the key models and underlying signals used by AI systems. The cards also explain the actions that individuals can take to influence the system, control their experience, and possibly impact the way the system delivers content. The controls listed in the system cards may not be available on all devices or to everyone.

The future of AI system cards

We will continue to incorporate feedback from diverse audiences to improve our products and empower people who use them. In our research we have learned that people want to explore system cards when the information provided is relevant to them, with a mix of text and visuals, so we are working on making system cards easier to find and understand.