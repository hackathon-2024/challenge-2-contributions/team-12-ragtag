EU Monthly Active Users
-----------------------

In accordance with the Digital Services Act (DSA), Microsoft Ireland Operations Limited publishes information semi-annually on its average monthly active users in the European Union, calculated over a six-month period. For the six-month period ending June 30, 2023, Microsoft Bing had approximately 119 million average monthly active users. This information was compiled pursuant to the DSA and thus may differ from other user metrics published by Microsoft Bing. 

DSA Point of Contact – European Commission and Member State Authorities
-----------------------------------------------------------------------

Pursuant to Article 11 of the DSA, Microsoft Ireland Operations Limited has designated [DigitalServicesAct@microsoft.com](mailto:DigitalServicesAct@microsoft.com) as the single point of contact for direct communications with the European Commission, Member States’ Authorities, and the European Board for Digital Services in connection with the application of the DSA. English is the preferred language for communication with this point of contact.

When sending messages to [DigitalServicesAct@microsoft.com](mailto:DigitalServicesAct@microsoft.com) please be sure to include your full name and the name of the EU-based authority on whose behalf you are contacting us. We’ll also need an email address to contact you, which should be associated with the relevant EU-based authority. 

This point of contact is reserved for engagement with the authorities listed above. For other types of inquiries, please use the mechanisms described below. 

Point of Contact for Bing Users
-------------------------------

Bing offers a variety of ways for users to contact us.  If you have a concern about particular URLs or other information you encounter on Bing, you may report these to Bing using our [Report a Concern tool](https://go.microsoft.com/fwlink?LinkId=850876). Feedback about Bing can also be submitted directly using the "Feedback” link located at the bottom of most Bing search pages. Additional information is available at our “[How to report a concern or Contact Bing](https://support.microsoft.com/en-gb/topic/how-to-report-a-concern-or-contact-bing-1831f0fe-3c4d-46ae-8e57-16c487715729)” help page. 

EU Advertising Repository
-------------------------

Please visit the [Microsoft Ad Library](https://adlibrary.ads.microsoft.com/) for a repository of advertisements served by the Microsoft Advertising Network to Bing users located in the European Union.