# Resource URL: https://www.tiktok.com/transparency/fr-fr/government-removal-requests-2023-1/
Demandes de retrait émanant des autorités publiques

_1 janvier 2023 – 30 juin 2023_

_Date de publication : 10 novembre 2023_

### **À propos de ce rapport**

TikTok repose sur la créativité de notre communauté et nous nous employons à encourager et protéger l’expression personnelle et la dimension divertissante de la plateforme. Toutefois, dans certains cas, nous retirerons ou restreindrons le contenu signalé par des organismes publics. Nous traitons les demandes qui nous sont adressées par les voies appropriées et lorsque la loi l’exige. Lorsque nous recevons de telles demandes de la part des pouvoirs publics, nous les étudions et prenons des mesures relatives au contenu signalé dans le respect de nos [Règles communautaires](https://www.tiktok.com/community-guidelines), de nos [Conditions générales](https://www.tiktok.com/legal/terms-of-service) et des lois applicables. Si nous estimons qu’une demande n’enfreint pas les dispositions définies dans nos Règles communautaires mais enfreint la législation en vigueur, nous pourrons imposer des restrictions sur la diffusion du contenu signalé dans le pays où celui-ci est considéré comme contraire à la loi. Si nous estimons qu’une demande n’est pas justifiée sur le plan juridique ou qu’il n’y a pas infraction de nos Règles communautaires, de nos Conditions générales ou de la législation en vigueur, nous nous réservons le droit de rejeter ladite demande. Les sections ci-dessous indiquent le volume des demandes de retrait ou de restriction reçues de gouvernements et la nature de la réponse donnée par TikTok.

### **Données les plus récentes**

[![download](https://sf16-website-login.neutral.ttwstatic.com/obj/tiktok_web_login_static/websites/static/images/icon-download-c0614844c2e22b8aede8367a66bcdae1.svg)Télécharger](https://sf16-va.tiktokcdn.com/obj/eden-va2/uhsllrta/H1%20%2723%20LIGR/gov/2023H1_raw_data_gov_French.csv)

#### **Demandes de retrait ou de restriction de contenu ou de comptes, émanant de gouvernements**

_REMARQUE : TikTok n’a pas reçu de demande de retrait ou de restriction de contenu ou de compte émanant de gouvernements de pays ou de marchés autres que ceux indiqués ci-dessus._

#### **Définitions**

* **Total des demandes reçues :** Les demandes de retrait ou de restriction de contenu ou de comptes émanant de gouvernements, y compris les demandes accompagnées d’adresses URL inexactes.
* **Total du contenu reçu :** Adresses URL de contenus valides demandées, à l’exclusion de toute forme acceptable de compte (c’est-à-dire URL, identifiants uniques et noms d’utilisateurs). Nous vérifions toutes les demandes relatives à du contenu pour détecter les adresses URL inexactes, les demandes en double, les demandes acheminées vers différents canaux et les demandes soumises avec des informations insuffisantes pour en déterminer la validité.
* **Contenu sanctionné pour infraction aux Règles communautaires :** Adresses URL de contenus valides vérifiées et sanctionnées pour violation de nos Règles communautaires.
* **Contenu sanctionné pour infraction aux lois (locales) :** Toutes les adresses URL de contenus valides vérifiées et sanctionnées pour violation d’une loi locale.
* **Contenu non sanctionné :** Adresses URL de contenus valides vérifiées et jugées comme n’enfreignant pas les Règles communautaires et les Conditions de services de TikTok ou les lois locales.
* **Total des comptes reçus :** Adresses URL de comptes valides et toute autre forme acceptable d’un compte (c’est-à-dire identifiants uniques et noms d’utilisateurs). Nous vérifions toutes les demandes relatives à du contenu pour détecter les adresses URL inexactes, les demandes en double, les demandes acheminées vers différents canaux et les demandes soumises avec des informations insuffisantes pour en déterminer la validité.
* **Comptes sanctionnés pour infraction aux Règles communautaires :** Adresses URL de comptes valides examinées et sanctionnées pour infraction aux Règles communautaires.
* **Comptes sanctionnés pour infraction aux lois (locales) :** Adresses URL de comptes valides examinées et sanctionnées pour infraction à une loi locale.
* **Comptes non sanctionnés :** Adresses URL de comptes valides examinées et jugées comme n’enfreignant pas les Règles communautaires de TikTok ou les lois locales.
* **Taux de retrait :** Taux de contenus ou de comptes que TikTok a supprimés ou restreint en réponse à des demandes émanant de gouvernements.

### **Autres rapports**

* [Rapport sur l’application des Règles communautaires](https://www.tiktok.com/transparency/fr-fr/community-guidelines-enforcement)
* [Rapport sur les demandes de retrait de contenus pour infractions aux lois sur la propriété intellectuelle](https://www.tiktok.com/transparency/fr-fr/intellectual-property-removal-requests)
* [Rapport sur les demandes de renseignements](https://www.tiktok.com/transparency/fr-fr/information-requests)

#### Cela a-t-il été utile ?

![thumps up](https://sf16-website-login.neutral.ttwstatic.com/obj/tiktok_web_login_static/websites/static/images/thumbs-up-80984a582e54af0b7149496dd4ede2a6.png)Oui![thumps down](https://sf16-website-login.neutral.ttwstatic.com/obj/tiktok_web_login_static/websites/static/images/thumbs-down-e0c9a7a1b1ea3c6ed439e5bf9a7e71bd.png)Non

# Resource URL: https://www.tiktok.com/transparency/fr-fr/government-removal-requests-2022-2/
Demandes de retrait émanant des autorités publiques

_1er juillet 2022 – 31 décembre 2022_  
_Publication du 15 mai 2023_

### **À propos de ce rapport**

TikTok repose sur la créativité de notre communauté et nous nous employons à encourager et protéger l’expression personnelle et la dimension divertissante de la plateforme. Toutefois, dans certains cas, nous retirerons ou restreindrons le contenu signalé par des organismes publics. Nous traitons les demandes qui nous sont adressées par les voies appropriées et lorsque la loi l’exige. Lorsque nous recevons de telles demandes de la part des pouvoirs publics, nous les étudions et prenons des mesures relatives au contenu signalé dans le respect de nos [Règles communautaires](https://www.tiktok.com/community-guidelines), de nos [Conditions générales](https://www.tiktok.com/legal/terms-of-service) et des lois applicables. Si nous estimons qu’une demande n’enfreint pas les dispositions définies dans nos Règles communautaires mais enfreint la législation en vigueur, nous pourrons imposer des restrictions sur la diffusion du contenu signalé dans le pays où celui-ci est considéré comme contraire à la loi. Si nous estimons qu’une demande n’est pas justifiée sur le plan juridique ou qu’il n’y a pas infraction de nos Règles communautaires, de nos Conditions générales ou de la législation en vigueur, nous nous réservons le droit de rejeter ladite demande.

Les sections ci-dessous indiquent le volume des demandes de retrait ou de restriction reçues de gouvernements et la nature de la réponse donnée par TikTok.

### **Analyse**

Au cours du second semestre 2022, nous avons reçu moins de demandes de suppression ou de restriction de contenus ou de comptes que lors de notre dernier rapport. Nous pensons que cela est dû en partie à la suspension de la diffusion en direct et des publications de nouveaux contenus sur notre service vidéo en Russie, qui a entraîné une baisse globale du nombre total de demandes reçues.

### **Données les plus récentes**

[![download](https://sf16-website-login.neutral.ttwstatic.com/obj/tiktok_web_login_static/websites/static/images/icon-download-c0614844c2e22b8aede8367a66bcdae1.svg)Télécharger](https://sf16-va.tiktokcdn.com/obj/eden-va2/nuvlojeh7ryht/Transparency_LIPGR_2022H2-2-Gov/2022H2_raw_data_gov_French.csv)

#### **Demandes de retrait ou de restriction de contenu ou de comptes, émanant de gouvernements**

_REMARQUE : TikTok n’a pas reçu de demande de retrait ou de restriction de contenu ou de compte émanant de gouvernements de pays ou de marchés autres que ceux indiqués ci-dessus._

#### **Définitions**

* **Total des demandes reçues :** Les demandes de retrait ou de restriction de contenu ou de comptes émanant de gouvernements, y compris les demandes accompagnées d’adresses URL inexactes.
* **Total du contenu reçu :** Adresses URL de contenus valides demandées, à l’exclusion de toute forme acceptable de compte (c’est-à-dire URL, identifiants uniques et noms d’utilisateurs). Nous vérifions toutes les demandes relatives à du contenu pour détecter les adresses URL inexactes, les demandes en double, les demandes acheminées vers différents canaux et les demandes soumises avec des informations insuffisantes pour en déterminer la validité.
* **Contenu sanctionné pour infraction aux Règles communautaires :** Adresses URL de contenus valides vérifiées et sanctionnées pour violation de nos Règles communautaires.
* **Contenu sanctionné pour infraction aux lois (locales) :** Toutes les adresses URL de contenus valides vérifiées et sanctionnées pour violation d’une loi locale.
* **Contenu non sanctionné :** Adresses URL de contenus valides vérifiées et jugées comme n’enfreignant pas les Règles communautaires et les Conditions de services de TikTok ou les lois locales.
* **Total des comptes reçus :** Adresses URL de comptes valides et toute autre forme acceptable d’un compte (c’est-à-dire identifiants uniques et noms d’utilisateurs). Nous vérifions toutes les demandes relatives à du contenu pour détecter les adresses URL inexactes, les demandes en double, les demandes acheminées vers différents canaux et les demandes soumises avec des informations insuffisantes pour en déterminer la validité.
* **Comptes sanctionnés pour infraction aux Règles communautaires :** Adresses URL de comptes valides examinées et sanctionnées pour infraction aux Règles communautaires.
* **Comptes sanctionnés pour infraction aux lois (locales) :** Adresses URL de comptes valides examinées et sanctionnées pour infraction à une loi locale.
* **Comptes non sanctionnés :** Adresses URL de comptes valides examinées et jugées comme n’enfreignant pas les Règles communautaires de TikTok ou les lois locales.
* **Taux de retrait :** Taux de contenus ou de comptes que TikTok a supprimés ou restreint en réponse à des demandes émanant de gouvernements.

### **Autres rapports**

* [Rapport sur l’application des Règles communautaires](https://www.tiktok.com/transparency/fr-fr/community-guidelines-enforcement)
* [Rapport sur les demandes de retrait de contenus pour infractions aux lois sur la propriété intellectuelle](https://www.tiktok.com/transparency/fr-fr/intellectual-property-removal-requests)
* [Rapport sur les demandes de renseignements](https://www.tiktok.com/transparency/fr-fr/information-requests)

#### Cela a-t-il été utile ?

![thumps up](https://sf16-website-login.neutral.ttwstatic.com/obj/tiktok_web_login_static/websites/static/images/thumbs-up-80984a582e54af0b7149496dd4ede2a6.png)Oui![thumps down](https://sf16-website-login.neutral.ttwstatic.com/obj/tiktok_web_login_static/websites/static/images/thumbs-down-e0c9a7a1b1ea3c6ed439e5bf9a7e71bd.png)Non

# Resource URL: https://www.tiktok.com/transparency/fr-fr/government-removal-requests-2022-1/
Demandes de retrait émanant des autorités publiques

_1 janvier – 30 juin 2022_  
_Date de publication : 29 novembre 2022_

### **À propos de ce rapport**

TikTok repose sur la créativité de notre communauté et nous nous employons à encourager et protéger l’expression personnelle et le divertissement. Toutefois, dans certains cas, nous retirerons ou restreindrons le contenu signalé par des organismes publics. Nous traitons les demandes qui nous sont adressées par les voies appropriées et lorsque la loi l’exige. Lorsque nous recevons de telles demandes de la part des pouvoirs publics, nous les étudions et prenons des mesures relatives au contenu signalé dans le respect de nos [Règles communautaires](https://www.tiktok.com/community-guidelines), de nos [Conditions générales](https://www.tiktok.com/legal/terms-of-service) et des lois applicables. Si nous estimons qu’une demande n’enfreint pas les dispositions définies dans nos Règles communautaires mais enfreint la législation en vigueur, nous pourrons imposer des restrictions sur la diffusion du contenu signalé dans le pays où celui-ci est considéré comme contraire à la loi. Si nous estimons qu’une demande n’est pas justifiée sur le plan juridique ou qu’il n’y a pas infraction de nos Règles communautaires, de nos Conditions générales ou de la législation en vigueur, nous nous réservons le droit de rejeter ladite demande.

Les sections ci-dessous indiquent le volume des demandes de retrait ou de restriction reçues de gouvernements et la nature de la réponse donnée par TikTok.

### **Analyse**

Un plus grand nombre de marchés ont fait des demandes de retrait par rapport à notre dernière période de rapport. L’assouplissement des restrictions COVID-19 sur certains marchés a entraîné une diminution des volumes de demandes, tandis que d’autres marchés en conflit ont vu leurs demandes de retrait augmenter.

### **Données les plus récentes**

[![download](https://sf16-website-login.neutral.ttwstatic.com/obj/tiktok_web_login_static/websites/static/images/icon-download-c0614844c2e22b8aede8367a66bcdae1.svg)Télécharger](https://sf16-va.tiktokcdn.com/obj/eden-va2/nuvlojeh7ryht/Transparency_LIPGR_2022H2/French_LIPGR_2022_H1.xlsx)

#### **Demandes de retrait ou de restriction de contenu ou de comptes, émanant de gouvernements**

_REMARQUE : en mars 2022, TikTok a suspendu le livestreaming et la création de nouvelles vidéos en Russie. Par conséquent, la grande majorité des demandes de retrait du gouvernement russe que nous avons reçues au cours de cette période de rapport concernaient des contenus et des comptes créés avant la guerre. TikTok n’a pas reçu de demande de retrait ou de restriction de contenu ou de compte émanant de gouvernements de pays ou de marchés autres que ceux indiqués ci-dessus._

#### **Définitions**

* **Total des demandes reçues :** Les demandes de retrait ou de restriction de contenu ou de comptes émanant de gouvernements, y compris les demandes accompagnées d’adresses URL inexactes.
* **Total du contenu reçu :** Adresses URL de contenus valides demandées, à l’exclusion de toute forme acceptable de compte (c’est-à-dire URL, identifiants uniques et noms d’utilisateurs). Nous vérifions toutes les demandes relatives à du contenu pour détecter les adresses URL inexactes, les demandes en double, les demandes acheminées vers différents canaux et les demandes soumises avec des informations insuffisantes pour en déterminer la validité.
* **Contenu sanctionné pour infraction aux Règles communautaires :** Adresses URL de contenus valides vérifiées et sanctionnées pour violation de nos Règles communautaires.
* **Contenu sanctionné pour infraction aux lois (locales) :** Toutes les adresses URL de contenus valides vérifiées et sanctionnées pour violation d’une loi locale.
* **Contenu non sanctionné :** Adresses URL de contenus valides vérifiées et jugées comme n’enfreignant pas les Règles communautaires et les Conditions de services de TikTok ou les lois locales.
* **Total des comptes reçus :** Adresses URL de comptes valides et toute autre forme acceptable d’un compte (c’est-à-dire identifiants uniques et noms d’utilisateurs). Nous vérifions toutes les demandes relatives à du contenu pour détecter les adresses URL inexactes, les demandes en double, les demandes acheminées vers différents canaux et les demandes soumises avec des informations insuffisantes pour en déterminer la validité.
* **Comptes sanctionnés pour infraction aux Règles communautaires :** Adresses URL de comptes valides examinées et sanctionnées pour infraction aux Règles communautaires.
* **Comptes sanctionnés pour infraction aux lois (locales) :** Adresses URL de comptes valides examinées et sanctionnées pour infraction à une loi locale.
* **Comptes non sanctionnés :** Adresses URL de comptes valides examinées et jugées comme n’enfreignant pas les Règles communautaires de TikTok ou les lois locales.
* **Taux de retrait :** Taux de contenus ou de comptes que TikTok a supprimés ou restreint en réponse à des demandes émanant de gouvernements.

### **Autres rapports**

* [Rapport sur l’application des Règles communautaires](https://www.tiktok.com/transparency/fr-fr/community-guidelines-enforcement)
* [Rapport sur les demandes de retrait de contenus pour infractions aux lois sur la propriété intellectuelle](https://www.tiktok.com/transparency/fr-fr/intellectual-property-removal-requests)
* [Rapport sur les demandes de renseignements](https://www.tiktok.com/transparency/fr-fr/information-requests)

#### Cela a-t-il été utile ?

![thumps up](https://sf16-website-login.neutral.ttwstatic.com/obj/tiktok_web_login_static/websites/static/images/thumbs-up-80984a582e54af0b7149496dd4ede2a6.png)Oui![thumps down](https://sf16-website-login.neutral.ttwstatic.com/obj/tiktok_web_login_static/websites/static/images/thumbs-down-e0c9a7a1b1ea3c6ed439e5bf9a7e71bd.png)Non

# Resource URL: https://www.tiktok.com/transparency/fr-fr/government-removal-requests-2021-2/
Demandes de retrait émanant des autorités publiques

_1er juillet – 31 décembre 2021_  
_Date de publication : 17 mai 2022_

### À propos de ce rapport

TikTok repose sur la créativité de notre communauté et nous nous employons à encourager et protéger l’expression personnelle et le divertissement. Toutefois, dans certains cas, nous retirerons ou restreindrons le contenu signalé par des organismes publics. Nous traitons les demandes qui nous sont adressées par les voies appropriées et lorsque la loi l’exige. Lorsque nous recevons de telles demandes de la part des pouvoirs publics, nous les étudions et prenons des mesures relatives au contenu signalé dans le respect de nos [Règles communautaires](https://www.tiktok.com/community-guidelines), de nos [Conditions générales](https://www.tiktok.com/legal/terms-of-service) et des lois applicables. Si nous estimons qu’une demande n’enfreint pas les dispositions définies dans nos Règles communautaires mais enfreint la législation en vigueur, nous pourrons imposer des restrictions sur la diffusion du contenu signalé dans le pays où celui-ci est considéré comme contraire à la loi. Si nous estimons qu’une demande n’est pas justifiée sur le plan juridique ou qu’il n’y a pas infraction de nos Règles communautaires, de nos Conditions générales ou de la législation en vigueur, nous nous réservons le droit de rejeter ladite demande.

Les sections ci-dessous indiquent le volume des demandes de retrait ou de restriction reçues de gouvernements et la nature de la réponse donnée par TikTok.

### Données les plus récentes

[![download](https://sf16-website-login.neutral.ttwstatic.com/obj/tiktok_web_login_static/websites/static/images/icon-download-c0614844c2e22b8aede8367a66bcdae1.svg)Télécharger](https://sf16-va.tiktokcdn.com/obj/eden-va2/uhaeh7pflk/Transparency_LIPGR_2021_H2/French_LIPGR_2021_H2.xlsx)

#### Demandes de retrait ou de restriction de contenu ou de comptes, émanant de gouvernements

_REMARQUE : TikTok n’a pas reçu de demande de retrait ou de restriction de contenu ou de compte émanant de gouvernements de pays ou de marchés autres que ceux indiqués ci-dessus._

#### Définitions

* **Total des demandes reçues :** Les demandes de retrait ou de restriction de contenu ou de comptes émanant de gouvernements, y compris les demandes accompagnées d’adresses URL inexactes.
* **Total du contenu reçu :** Adresses URL de contenus valides demandées, à l’exclusion de toute forme acceptable de compte (c’est-à-dire URL, identifiants uniques et noms d’utilisateurs). Nous vérifions toutes les demandes relatives à du contenu pour détecter les adresses URL inexactes, les demandes en double, les demandes acheminées vers différents canaux et les demandes soumises avec des informations insuffisantes pour en déterminer la validité.
* **Contenu sanctionné pour infraction aux Règles communautaires :** Adresses URL de contenus valides vérifiées et sanctionnées pour violation de nos Règles communautaires.
* **Contenu sanctionné pour infraction aux lois (locales) :** Toutes les adresses URL de contenus valides vérifiées et sanctionnées pour violation d’une loi locale.
* **Contenu non sanctionné :** Adresses URL de contenus valides vérifiées et jugées comme n’enfreignant pas les Règles communautaires et les Conditions de services de TikTok ou les lois locales.
* **Total des comptes reçus :** Adresses URL de comptes valides et toute autre forme acceptable d’un compte (c’est-à-dire identifiants uniques et noms d’utilisateurs). Nous vérifions toutes les demandes relatives à du contenu pour détecter les adresses URL inexactes, les demandes en double, les demandes acheminées vers différents canaux et les demandes soumises avec des informations insuffisantes pour en déterminer la validité.
* **Comptes sanctionnés pour infraction aux Règles communautaires :** Adresses URL de comptes valides examinées et sanctionnées pour infraction aux Règles communautaires.
* **Comptes sanctionnés pour infraction aux lois (locales) :** Adresses URL de comptes valides examinées et sanctionnées pour infraction à une loi locale.
* **Comptes non sanctionnés :** Adresses URL de comptes valides examinées et jugées comme n’enfreignant pas les Règles communautaires de TikTok ou les lois locales.
* **Taux de retrait :** Taux de contenus ou de comptes que TikTok a supprimés ou restreint en réponse à des demandes émanant de gouvernements.

### Autres rapports

* [Rapport sur l’application des Règles communautaires](https://www.tiktok.com/transparency/fr-fr/community-guidelines-enforcement)
* [Rapport sur les demandes de retrait de contenus pour infractions aux lois sur la propriété intellectuelle](https://www.tiktok.com/transparency/fr-fr/intellectual-property-removal-requests)
* [Rapport sur les demandes de renseignements](https://www.tiktok.com/transparency/fr-fr/information-requests)

#### Cela a-t-il été utile ?

![thumps up](https://sf16-website-login.neutral.ttwstatic.com/obj/tiktok_web_login_static/websites/static/images/thumbs-up-80984a582e54af0b7149496dd4ede2a6.png)Oui![thumps down](https://sf16-website-login.neutral.ttwstatic.com/obj/tiktok_web_login_static/websites/static/images/thumbs-down-e0c9a7a1b1ea3c6ed439e5bf9a7e71bd.png)Non

# Resource URL: https://www.tiktok.com/transparency/fr-fr/government-removal-requests-2021-1/
Demandes de retrait émanant des autorités publiques

_1er janvier 2021-30 juin 2021  
__Publication : 2 décembre 2021_

### À propos de ce rapport

TikTok repose sur la créativité de notre communauté et nous nous employons à encourager et protéger l’expression personnelle et le divertissement. Toutefois, dans certains cas, nous retirerons ou restreindrons le contenu signalé par des organismes gouvernementaux. Nous honorons les demandes de retrait ou de restriction de contenu qui nous sont adressées par les voies indiquées dans notre Centre d’aide et lorsque la loi l’exige. Lorsque nous recevons de telles demandes émanant de gouvernements, nous les étudions et prenons des mesures relatives au contenu signalé dans le respect de nos [Règles communautaires](https://www.tiktok.com/community-guidelines), de nos [Conditions générales](https://www.tiktok.com/legal/terms-of-service) et des lois applicables. Si nous pensons qu’une demande n’est pas légalement recevable ou n’enfreint pas nos normes, nous pourrions restreindre la disponibilité du contenu signalé dans le pays où il serait considéré comme illégal ou encore, nous pourrions rejeter la demande.

Les sections ci-dessous indiquent le volume des demandes de retrait ou de restriction reçues de gouvernement et la nature de la réponse donnée par TikTok.

### Données les plus récentes

[![download](https://sf16-website-login.neutral.ttwstatic.com/obj/tiktok_web_login_static/websites/static/images/icon-download-c0614844c2e22b8aede8367a66bcdae1.svg)Télécharger](https://sf16-va.tiktokcdn.com/obj/eden-va2/nupfpxbog/French_LIPGR_2021_H1.xlsx)

#### Demandes de retrait ou de restriction de contenu ou de comptes, émanant de gouvernements

REMARQUE : TikTok n’a pas reçu de demande de retrait ou de restriction de contenu ou de compte émanant de gouvernements de pays ou de marchés autres que ceux indiqués ci-dessus.

#### Définitions

* **Total des demandes reçues** : Les demandes de retrait ou de restriction de contenu ou de comptes émanant de gouvernements, y compris les demandes accompagnées d’adresses URL inexactes.
* **Total du contenu reçu** : Adresses URL de contenus valides demandées, à l’exclusion de toute forme acceptable de compte (c’est-à-dire URL, identifiants uniques et noms d’utilisateurs). Nous vérifions toutes les demandes relatives à du contenu pour détecter les adresses URL inexactes, les demandes en double, les demandes acheminées vers différents canaux et les demandes soumises avec des informations insuffisantes pour en déterminer la validité.
* **Contenu sanctionné pour infraction aux Règles communautaires** : Adresses URL de contenus valides vérifiées et sanctionnées pour violation de nos Règles communautaires.
* **Contenu sanctionné pour infraction aux lois (locales)** : Toutes les adresses URL de contenus valides vérifiées et sanctionnées pour violation d’une loi locale.
* **Contenu non sanctionné** : Adresses URL de contenus valides vérifiées et jugées comme n’enfreignant pas les Règles communautaires et les Conditions de services de TikTok ou les lois locales.
* **Total des comptes reçus** : Adresses URL de comptes valides et toute autre forme acceptable d’un compte (c’est-à-dire identifiants uniques et noms d’utilisateurs). Nous vérifions toutes les demandes relatives à du contenu pour détecter les adresses URL inexactes, les demandes en double, les demandes acheminées vers différents canaux et les demandes soumises avec des informations insuffisantes pour en déterminer la validité.
* **Comptes sanctionnés pour infraction aux Règles communautaires** : Adresses URL de comptes valides examinées et sanctionnées pour infraction aux Règles communautaires.
* **Comptes sanctionnés pour infraction aux lois (locales)** : Adresses URL de comptes valides examinées et sanctionnées pour infraction à une loi locale.
* **Comptes non sanctionnés** : Adresses URL de comptes valides examinées et jugées comme n’enfreignant pas les Règles communautaires de TikTok ou les lois locales.
* **Taux de retrait** : Taux de contenus ou de comptes que TikTok a supprimés ou restreint en réponse à des demandes émanant de gouvernements.

### Autres rapports

* [Rapport sur l’application des Règles communautaires](https://www.tiktok.com/transparency/fr-fr/community-guidelines-enforcement)
* [Rapport de demandes de retrait de contenus pour infractions aux lois sur la propriété intellectuelle](https://www.tiktok.com/transparency/fr-fr/intellectual-property-removal-requests)
* [Rapport sur les demandes de renseignements](https://www.tiktok.com/transparency/fr-fr/information-requests)

#### Cela a-t-il été utile ?

![thumps up](https://sf16-website-login.neutral.ttwstatic.com/obj/tiktok_web_login_static/websites/static/images/thumbs-up-80984a582e54af0b7149496dd4ede2a6.png)Oui![thumps down](https://sf16-website-login.neutral.ttwstatic.com/obj/tiktok_web_login_static/websites/static/images/thumbs-down-e0c9a7a1b1ea3c6ed439e5bf9a7e71bd.png)Non

# Resource URL: https://www.tiktok.com/transparency/fr-fr/government-removal-requests-2020-2/
Demandes de retrait émanant des autorités publiques

_1e juillet – 31 décembre 2020_  
_Publié le 24 février 2021_

### À propos de ce rapport

Les rubriques ci-dessous donnent un aperçu du volume et de la nature des demandes juridiques que nous avons reçues au cours du second semestre 2020 et de la façon dont nous les avons traitées. Nous recevons des demandes des pouvoirs publics et autorités juridiques dans le monde entier. Nous honorons les demandes qui nous sont adressées par les voies appropriées et lorsque la loi l’exige.

Quand nous recevons des demandes des autorités publiques visant à restreindre ou à supprimer des contenus sur notre plateforme, conformément aux lois locales, nous examinons tous les documents conformément à nos Règles Communautaires, nos Conditions de services, ainsi que la législation en vigueur, et prenons les mesures appropriées. Si nous estimons qu’une demande n’est pas légalement recevable ou qu’elle ne va pas à l’encontre de nos règles, nous pouvons choisir de limiter la diffusion du contenu signalé dans le pays où il est présumé illégal ou bien choisir de ne prendre aucune mesure.

### Rapport sur les données

#### Demandes de retrait de contenu émanant d’autorités publiques

|     |     |     |     |     |
| --- | --- | --- | --- | --- |
| **Pays / Marché** | **Demandes de gouvernements** | **Total de comptes spécifiés** | **Comptes supprimés ou restreints** | **Contenus supprimés ou restreints** |
| Arménie | 4   | 20  | 0   | 8   |
| Australie | 32  | 98  | 74  | 16  |
| Brésil | 1   | 1   | 1   | 1   |
| Bangladesh | 1   | 1   | 1   | 0   |
| Belgique | 1   | 1   | 1   | 0   |
| Canada | 4   | 4   | 4   | 0   |
| Chypre | 1   | 1   | 0   | 0   |
| Egypte | 1   | 1   | 1   | 1   |
| Estonie | 1   | 6   | 6   | 0   |
| Finlande | 1   | 1   | 1   | 0   |
| France | 9   | 20  | 6   | 23  |
| Allemagne | 1   | 1   | 1   | 0   |
| Indonésie | 2   | 3   | 0   | 26  |
| Israël | 15  | 15  | 10  | 10  |
| Islande | 1   | 1   | 1   | 1   |
| Japon | 2   | 0   | 0   | 8   |
| Malaisie | 2   | 2   | 2   | 0   |
| Nouvelle Zélande | 4   | 4   | 4   | 0   |
| Norvège | 27  | 61  | 56  | 10  |
| Népal | 15  | 15  | 11  | 14  |
| Pakistan | 97  | 50  | 24  | 14263 |
| Russie | 135 | 375 | 94  | 429 |
| Sri Lanka | 10  | 11  | 6   | 4   |
| Suède | 2   | 2   | 1   | 1   |
| Thailande | 10  | 1   | 0   | 24  |
| Turquie | 13  | 14  | 6   | 16  |
| Taiwan | 1   | 39  | 21  | 0   |
| Vietnam | 2   | 2   | 1   | 0   |
| Emirats Arabes Unis | 2   | 4   | 1   | 25  |
| Royaume-Uni | 4   | 4   | 4   | 0   |
| Etats-Unis | 5   | 5   | 4   | 0   |
| Uzbekistan | 6   | 98  | 34  | 82  |

### Autres rapports

* [Rapport sur l’application des Règles communautaires](https://www.tiktok.com/transparency/fr-fr/community-guidelines-enforcement)
* [Rapport de demandes de retrait de contenus pour infractions aux lois sur la propriété intellectuelle](https://www.tiktok.com/transparency/fr-fr/intellectual-property-removal-requests)
* [Rapport sur les demandes de renseignements](https://www.tiktok.com/transparency/fr-fr/information-requests)

#### Cela a-t-il été utile ?

![thumps up](https://sf16-website-login.neutral.ttwstatic.com/obj/tiktok_web_login_static/websites/static/images/thumbs-up-80984a582e54af0b7149496dd4ede2a6.png)Oui![thumps down](https://sf16-website-login.neutral.ttwstatic.com/obj/tiktok_web_login_static/websites/static/images/thumbs-down-e0c9a7a1b1ea3c6ed439e5bf9a7e71bd.png)Non

# Resource URL: https://www.tiktok.com/transparency/fr-fr/government-removal-requests-2020-1/
Demandes de retrait émanant des autorités publiques

_Du 1e janvier au 30 juin 2020_  
_Publié le 22 Septembre 2020_

### À propos de ce rapport

Les sections ci-dessous présentent le volume et la nature des demandes reçues de la part des gouvernements et autorités locales au cours du premier semestre 2020, et la nature de notre réponse. Nous recevons des demandes émanants de gouvernement et d’agences gouvernementales de partout dans le monde entier. Nous honorons les demandes qui nous sont adressées dans le respect des règles et quand la loi l’exige.

De temps en temps, les autorités publiques nous soumettent des demandes en vue de limiter ou supprimer de notre plateforme du contenu contrevenant à la législation locale. Nous examinons ces éléments conformément à nos Règles Communautaires, à nos Conditions de service, ainsi qu’à la législation en vigueur et prenons la décision adaptée. Si nous pensons qu’un signalement n’est pas légalement recevable ou ne va pas à l’encontre de nos standards, nous pouvons décider soit de limiter la disponibilité du contenu signalé dans le pays où elle est supposément illégale, soit de ne prendre aucune mesure.

### Rapport sur les données

#### Demandes de retrait partiel de contenu émanant d’autorités publiques

|     |     |     |     |     |
| --- | --- | --- | --- | --- |
| **Pays / Marché** | **Demande de gouvernements** | **Total de comptes spécifiés** | **Comptes supprimés ou restreints** | **Contenus supprimés ou restreints** |
| Australie | 13  | 14  | 8   | 5   |
| Belgique | 10  | 10  | 2   | 8   |
| Canada | 1   | 1   | 0   | 1   |
| Allemagne | 4   | 4   | 1   | 3   |
| Danemark | 1   | 1   | 0   | 1   |
| Inde | 55  | 244 | 8   | 225 |
| Norvège | 14  | 24  | 17  | 1   |
| Nouvelle Zélande | 1   | 1   | 0   | 1   |
| Pakistan | 4   | 40  | 2   | 129 |
| Russie | 15  | 259 | 9   | 296 |
| Singapour | 1   | 2   | 2   | 0   |
| Turquie | 9   | 36  | 2   | 34  |
| Taiwan | 1   | 6   | 0   | 0   |
| Royaume-Uni | 2   | 5   | 2   | 3   |
| Etats-Unis | 4   | 4   | 2   | 4   |

### Autres rapports

* [Rapport sur l’application des Règles communautaires](https://www.tiktok.com/transparency/fr-fr/community-guidelines-enforcement)
* [Rapport de demandes de retrait de contenus pour infractions aux lois sur la propriété intellectuelle](https://www.tiktok.com/transparency/fr-fr/intellectual-property-removal-requests)
* [Rapport sur les demandes de renseignements](https://www.tiktok.com/transparency/fr-fr/information-requests)

#### Cela a-t-il été utile ?

![thumps up](https://sf16-website-login.neutral.ttwstatic.com/obj/tiktok_web_login_static/websites/static/images/thumbs-up-80984a582e54af0b7149496dd4ede2a6.png)Oui![thumps down](https://sf16-website-login.neutral.ttwstatic.com/obj/tiktok_web_login_static/websites/static/images/thumbs-down-e0c9a7a1b1ea3c6ed439e5bf9a7e71bd.png)Non

# Resource URL: https://www.tiktok.com/transparency/fr-fr/government-removal-requests-2019-2/
Demandes de retrait émanant des autorités publiques

_1er juillet 2019 –31 décembre 2019 Date de publication : 9 juin 2020_

### À propos de ce rapport

Les sections ci-dessous présentent le volume des demandes légales reçues des gouvernements et autorités locales dans le monde entier au cours du second semestre 2019 et la nature de notre réponse. Nous respectons les droits et la vie privée de nos utilisateurs. C’est pourquoi nous demandons aux autorités publiques de soumettre par écrit leur demande accompagnée des documents légaux appropriés. Nous ne prenons pas en compte des demandes qui ne sont effectuée par les voies appropriées. De temps en temps, les autorités publiques nous soumettent des demandes en vue de retirer du contenu de notre plateforme comme par exemple concernant des législations locales interdisant les comportements obscènes, les discours haineux, des contenus d’adulte, etc… Nous examinons ces éléments conformément à nos Règles Communautaires, à nos Conditions de services, ainsi qu’à la législation en vigueur et prenons la décision adaptée. Si nous pensons qu’un signalement n’est pas légalement recevable ou ne va pas à l’encontre de nos standards, nous ne prenons aucune mesure.

### Rapport sur les données

#### Demandes de retrait de contenu émanants d’autorités publiques

|     |     |     |     |     |
| --- | --- | --- | --- | --- |
| **Pays / Marché** | **Demande de gouvernements** | **Total de comptes spécifiés** | **Comptes supprimés ou restreints** | **Contenus supprimés ou restreints** |
| Australie | 6   | 6   | 5   | 1   |
| Belgique | 1   | 1   | 0   | 0   |
| Canada | 1   | 8   | 0   | 8   |
| Chypre | 1   | 1   | 0   | 1   |
| Inde | 30  | 35  | 28  | 19  |
| Norvège | 1   | 5   | 5   | 0   |
| Pays-Bas | 1   | 78  | 78  | 0   |
| Sri Lanka | 1   | 1   | 1   | 0   |
| Turquie | 2   | 4   | 3   | 16  |
| États Unis | 1   | 1   | 1   | 0   |

_Remarque_ _: TikTok n’a reçu aucune demande de la part d’autorités publiques de retrait ou de restriction de contenu provenant de pays ou de marchés autres que ceux présents dans la liste ci-dessus._

### Autres rapports

* [Rapport sur l’application des Règles communautaires](https://www.tiktok.com/transparency/fr-fr/community-guidelines-enforcement)
* [Rapport de demandes de retrait de contenus pour infractions aux lois sur la propriété intellectuelle](https://www.tiktok.com/transparency/fr-fr/intellectual-property-removal-requests)
* [Rapport sur les demandes de renseignements](https://www.tiktok.com/transparency/fr-fr/information-requests)

#### Cela a-t-il été utile ?

![thumps up](https://sf16-website-login.neutral.ttwstatic.com/obj/tiktok_web_login_static/websites/static/images/thumbs-up-80984a582e54af0b7149496dd4ede2a6.png)Oui![thumps down](https://sf16-website-login.neutral.ttwstatic.com/obj/tiktok_web_login_static/websites/static/images/thumbs-down-e0c9a7a1b1ea3c6ed439e5bf9a7e71bd.png)Non

# Resource URL: https://www.tiktok.com/transparency/fr-fr/government-removal-requests-2019-1/
Demandes de retrait émanant des autorités publiques

_January 1, 2019 – June 30, 2019  
Released: December 30, 2019_

### About this report

We take any request from government bodies extremely seriously, and closely review each such request we receive to determine whether, for example, the request adheres to the required legal process or the content violates a local law. TikTok is committed to assisting law enforcement in appropriate circumstances while respecting the privacy and rights of our users.

The sections below provide insight into the requests we received from government bodies in the markets where the TikTok app operates during the first half of 2019. We did not receive any requests from countries other than those listed in the chart below. In addition, we have also included a description of our copyright take-down process.

### Latest data

#### Government requests for content removal

|     |     |     |     |     |
| --- | --- | --- | --- | --- |
| **Country** | **Government requests** | **Total accounts specified** | **Accounts removed or restricted** | **Content removed or restricted** |
| Australia | 2   | 2   | 2   | 0   |
| France | 2   | 2   | 2   | 0   |
| Germany | 1   | 1   | 0   | 1   |
| India | 11  | 9   | 8   | 4   |
| Israel | 1   | 1   | 0   | 1   |
| Italy | 1   | 1   | 1   | 0   |
| Japan | 3   | 5   | 4   | 1   |
| United Kingdom | 1   | 1   | 1   | 0   |
| United States | 6   | 7   | 7   | 1   |

_Note: TikTok did not receive any government requests to remove or restrict content from countries other than those on the list above._

### Other reports

* [Rapport sur l’application des Règles communautaires](https://www.tiktok.com/transparency/fr-fr/community-guidelines-enforcement)
* [Rapport de demandes de retrait de contenus pour infractions aux lois sur la propriété intellectuelle](https://www.tiktok.com/transparency/fr-fr/intellectual-property-removal-requests)
* [Rapport sur les demandes de renseignements](https://www.tiktok.com/transparency/fr-fr/information-requests)

#### Cela a-t-il été utile ?

![thumps up](https://sf16-website-login.neutral.ttwstatic.com/obj/tiktok_web_login_static/websites/static/images/thumbs-up-80984a582e54af0b7149496dd4ede2a6.png)Oui![thumps down](https://sf16-website-login.neutral.ttwstatic.com/obj/tiktok_web_login_static/websites/static/images/thumbs-down-e0c9a7a1b1ea3c6ed439e5bf9a7e71bd.png)Non